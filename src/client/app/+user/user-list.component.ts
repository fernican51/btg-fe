import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { MD_CARD_DIRECTIVES } from '@angular2-material/card';
import { MD_ICON_DIRECTIVES, MdIconRegistry } from '@angular2-material/icon';

import { UserService } from './user.service';
import { User } from './user.model';
import { UserFormComponent } from './user-form.component';

@Component({
  moduleId: module.id,
  selector: 'sd-user-list',
  templateUrl: 'user-list.component.html',
  styleUrls: ['../shared/general/general.utilities.css', 'user-list.component.css'],
  directives: [MD_CARD_DIRECTIVES, UserFormComponent, MD_ICON_DIRECTIVES],
  viewProviders: [MdIconRegistry]
})
export class UserListComponent implements OnInit {
  userList: User[];
  newUser: User;
  displayAddUser: Boolean;

    constructor(private userService: UserService, private router: Router) { }

    ngOnInit() {
        this.displayAddUser = false;
        this.getAllItems();
        this.resetNewUser();
    }

    goToDetail(user: User) {
        this.router.navigate(['/user', user.id]);
    }

    addUser(user: User, userForm: UserFormComponent): void {
        this.userService
            .Add(this.newUser)
            .subscribe((data:User) => {
                this.resetUserForm(userForm);
                this.userList.push(data);
            },
            error => console.log(error),
            () => console.log('Add user completed'));
    }

    reset(userForm: UserFormComponent): void {
        this.resetUserForm(userForm);
    }

    cancel(userForm: UserFormComponent): void {
        this.hideAddUser();
        this.resetUserForm(userForm);
    }

    showAddUser(): void {
        this.displayAddUser = true;
    }

    hideAddUser(): void {
        this.displayAddUser = false;
    }

    private resetUserForm(userForm: UserFormComponent): void {
        this.resetNewUser();
        userForm.clear();
    }

    private getAllItems(): void {
    this.userService
        .GetAll()
        .subscribe((data:User[]) => this.userList = data,
            error => console.log(error),
            () => console.log('Get all Items complete'));
    }

    private resetNewUser(): void {
        this.newUser = new User();
    }
}
