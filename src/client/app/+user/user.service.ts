import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { User } from './user.model';
import { Configuration } from '../app.configuration';

@Injectable()
export class UserService {
    private actionUrl: string;
    private headers: Headers;

    constructor(private http: Http, private configuration: Configuration) {

        this.actionUrl = configuration.Server + 'users/';

        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
    }

    public GetAll = (): Observable<User[]> => {
        return this.http.get(this.actionUrl)
            .map((response: Response) => <User[]>response.json())
            .catch(this.handleError);
    }

    public GetSingle = (id: number): Observable<User> => {
        return this.http.get(this.actionUrl + id)
            .map((response: Response) => <User>response.json())
            .catch(this.handleError);
    }

    public Add = (user: User): Observable<User> => {
        let toAdd = JSON.stringify(user);

        return this.http.post(this.actionUrl, toAdd, { headers: this.headers })
            .map((response: Response) => <User>response.json())
            .catch(this.handleError);
    }

    public Update = (id: number, itemToUpdate: User): Observable<User> => {
        return this.http.put(this.actionUrl + id, JSON.stringify(itemToUpdate), { headers: this.headers })
            .map((response: Response) => <User>response.json())
            .catch(this.handleError);
    }

    public Delete = (id: number): Observable<Response> => {
        return this.http.delete(this.actionUrl + id)
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}
