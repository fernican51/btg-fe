import { RouterConfig } from '@angular/router';

import { UserListComponent, UserDetailComponent } from './index';

export const UserRoutes: RouterConfig = [
  {
    path: 'user',
    component: UserListComponent
  },
  {
    path: 'user/:id',
    component: UserDetailComponent
  }
];
