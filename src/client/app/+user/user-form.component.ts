import { Component, Input, Output, EventEmitter } from '@angular/core';

import { MD_INPUT_DIRECTIVES } from '@angular2-material/input';
import { MD_CARD_DIRECTIVES } from '@angular2-material/card';

import { User } from '../+user/user.model';

@Component({
  moduleId: module.id,
  selector: 'user-form',
  templateUrl: './user-form.component.html',
  directives: [MD_INPUT_DIRECTIVES, MD_CARD_DIRECTIVES],
  styleUrls: ['../shared/general/general.utilities.css', '../shared/form/form.utilities.css']
})
export class UserFormComponent {
  @Input()  user:User;
  @Input()  editable:Boolean;
	@Input()  primaryActionText:String;
	@Input()  secondaryActionText:String;
	@Output() onPrimaryClick = new EventEmitter<User>();
  @Output() onSecondaryClick = new EventEmitter<User>();
	@Output() onResetClick = new EventEmitter<User>();
  active =  true;
  roles  = [
    { id: 'developer', description: 'Developer' },
    { id: 'qa', description: 'QA' }
  ];

  onSecondaryAction() {
    this.onSecondaryClick.emit(this.user);
  }

  onPrimaryAction() {
    this.onPrimaryClick.emit(this.user);
  }

  onResetAction() {
    this.onResetClick.emit(this.user);
  }

  clear() {
    this.active = false;
    setTimeout(() => this.active = true, 0);
  }
}
