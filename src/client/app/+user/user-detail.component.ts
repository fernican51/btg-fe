import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { MD_CARD_DIRECTIVES } from '@angular2-material/card';

import { UserService } from './user.service';
import { User } from './user.model';
import { UserFormComponent } from './user-form.component';

@Component({
  moduleId: module.id,
  selector: 'sd-user-list',
  templateUrl: 'user-detail.component.html',
  directives: [MD_CARD_DIRECTIVES, UserFormComponent]
})
export class UserDetailComponent implements  OnInit, OnDestroy {
  userList: User[];
  userModel: User;
  editable: Boolean;
  private sub: any;
  private user: User;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _userService: UserService) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
     let id = +params['id'];
     this._userService
        .GetSingle(id)
        .subscribe((data:User) => {
          this.cancelEdit();
          this.user = data;
          this.userModel = Object.assign(new User(), this.user);
        },
            error => console.log(error),
            () => console.log('Get user completed!'));
   });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  goBack() {
    window.history.back();
  }

  delete() {
    this._userService
        .Delete(this.user.id)
        .subscribe(() => window.history.back(),
            error => console.log(error),
            () => console.log('Delete user completed!'));
  }

  save(user: User) {
    this._userService
        .Update(this.user.id, user)
        .subscribe((data:User) => {
          this.user = data;
          this.userModel = Object.assign(new User(), this.user);
        },
        error => console.log(error),
        () => console.log('Update user completed!'));
  }

  enableEdit() {
    this.editable = true;
  }

  cancelEdit() {
    this.editable = false;
  }

  reset(userForm: UserFormComponent) {
    this.userModel = Object.assign(new User(), this.user);
    userForm.clear();
  }
}
