/**
 * This barrel file provides the export for the lazy loaded AboutComponent.
 */
export * from './user-list.component';
export * from './user-detail.component';
export * from './user-form.component';
export * from './user.routes';

