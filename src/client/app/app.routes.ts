import { provideRouter, RouterConfig } from '@angular/router';

import { AboutRoutes } from './+about/index';
import { HomeRoutes } from './+home/index';
import { UserRoutes } from './+user/index';

const routes: RouterConfig = [
  ...HomeRoutes,
  ...AboutRoutes,
  ...UserRoutes
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes),
];
